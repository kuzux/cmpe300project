/* 
Student Name: Arda Cinar
Student Number: 2011400288 
Operating System: Linux [Ubuntu 13.10 64bit, gcc 4.8.1]
Compile Status: Compiling 
Program Status: Working 
Notes: The input file must NOT be followed by an empty space,
       it causes the list to be of size 3^n + 1, and causes some weird errors. 
*/ 

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

int numprocessors, rank;
vector<double> data;
enum MessageType {
    MT_SIZE,
    MT_CHILDREN,
    MT_DATA,
    MT_REPLY
};

// A : std::vector containing numbers to be scrambled. it is modified in-place
void sequential_scramble(vector<double>& A){
    int size = A.size();

    // base case
    if(size==3){
        // from 1 2 3
        //   to 2 3 1
        double tmp = A[0];
        A[0] = A[1];
        A[1] = A[2];
        A[2] = A[0];
        return;
    }

    int m = size/3;

    vector<double> a1(m);
    vector<double> a2(m);
    vector<double> a3(m);

    // split the list into three
    copy(A.begin(), A.begin()+m, a1.begin());
    copy(A.begin()+m, A.begin()+2*m, a2.begin());
    copy(A.begin()+2*m, A.end(), a3.begin());

    sequential_scramble(a1);
    sequential_scramble(a2);
    sequential_scramble(a3);

    // put the pieces back together
    copy(a3.begin(),a3.end(), A.begin());
    copy(a1.begin(),a1.end(), A.begin()+m);
    copy(a2.begin(),a2.end(), A.begin()+2*m);
}

// scrambles in-place.
// replies back to its parent
// children : number of child processors of the current processor
//            (both direct and indirect descendants)
// parent   : which processor has issued the scramble command to this processor. 
//            i.e if we received a message from another processor j and called scramble, parent is j
//            or if we've called parallel_scramble as a regular recursive call, parent = rank
// A        : an std::vector containing the data to be scramble. it is modified in-place.
void parallel_scramble(int children, int parent, vector<double>& A){
    int size = A.size();

    // check if we've run out of processors to distribute to
    if(children==1){
        sequential_scramble(data);
    } else {
        int offset = children/3;

        // calculate the ranks of the child processors (direct descendants)
        // for example, for children = 9 and rank = 0:
        // 0 1 2 3 4 5 6 7 8
        // ^     ^     ^
        int child1 = rank + offset;
        int child2 = rank + 2*offset;

        int m = size/3;
        
        // send the initial info (size and number of children) to child processes
        MPI_Send(&m, 1, MPI_INT, child1, MT_SIZE, MPI_COMM_WORLD);
        MPI_Send(&m, 1, MPI_INT, child2, MT_SIZE, MPI_COMM_WORLD);
        MPI_Send(&offset, 1, MPI_INT, child1, MT_CHILDREN, MPI_COMM_WORLD);
        MPI_Send(&offset, 1, MPI_INT, child2, MT_CHILDREN, MPI_COMM_WORLD);

        vector<double> a1(m);
        vector<double> a2(m);
        vector<double> a3(m);
        
        // partition data
        copy(A.begin(), A.begin()+m, a1.begin());
        copy(A.begin()+m, A.begin()+2*m, a2.begin());
        copy(A.begin()+2*m, A.end(), a3.begin());

        // send to children
        parallel_scramble(children/3, rank, a1);
        MPI_Send(&a2.front(), m, MPI_DOUBLE, child1, MT_DATA, MPI_COMM_WORLD);
        MPI_Send(&a3.front(), m, MPI_DOUBLE, child2, MT_DATA, MPI_COMM_WORLD);

        // receive from children
        MPI_Status status;
        MPI_Recv(&a2.front(), m, MPI_DOUBLE, child1, MT_REPLY, MPI_COMM_WORLD, &status);
        MPI_Recv(&a3.front(), m, MPI_DOUBLE, child2, MT_REPLY, MPI_COMM_WORLD, &status);

        // unify data
        copy(a3.begin(),a3.end(), A.begin());
        copy(a1.begin(),a1.end(), A.begin()+m);
        copy(a2.begin(),a2.end(), A.begin()+2*m);
    }

    // if you're not called from the same processor, send to parent
    if(parent != rank){
        MPI_Send(&A.front(), size, MPI_DOUBLE, parent, MT_REPLY, MPI_COMM_WORLD);
    }
}

void read(istream& in){
    while(!in.eof()){
        double t;
        in >> t;
        data.push_back(t);
    }
}

void write(ostream& out){
    for(int i = 0; i < data.size(); i++){
        out << data[i] << endl;
    }
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocessors);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if ( rank == 0 ) {        
        ifstream fin(argv[1]);
        read(fin);
        fin.close();

        // for testing sequential and parallel scrambles
        #ifdef SEQUENTIAL
            sequential_scramble(data);
        #else
            parallel_scramble(numprocessors, 0, data);
        #endif

        ofstream fout(argv[2]);
        write(fout);
        fout.close();
    } else {
        MPI_Status status;
        int size, children;

        // receive info from parent
        MPI_Recv(&size, 1, MPI_INT, MPI_ANY_SOURCE, MT_SIZE, MPI_COMM_WORLD, &status);
        MPI_Recv(&children, 1, MPI_INT, MPI_ANY_SOURCE, MT_CHILDREN, MPI_COMM_WORLD, &status);
        
        // find the parent of current processor
        int parent = status.MPI_SOURCE;

        // resize the input buffer
        data.resize(size);

        // receive data to buffer
        MPI_Recv(&data.front(), size, MPI_DOUBLE, MPI_ANY_SOURCE, MT_DATA, MPI_COMM_WORLD, &status);

        parallel_scramble(children, parent, data);
    }
    MPI_Finalize();
}
