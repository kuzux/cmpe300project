CC = mpicxx

main: main.cpp
	$(CC) main.cpp -o main

sequential: main.cpp
	$(CC) main.cpp -D SEQUENTIAL -o sequential

test: main
	mpiexec -np 9 ./main in.txt out.txt

seqtest: sequential
	./sequential in.txt out2.txt

report: report.tex
	xelatex report.tex
